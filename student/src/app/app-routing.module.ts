import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { authGuard } from './guard/auth.guard';

import { LoginComponent } from './login/login.component';


const routes: Routes = [

  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "admin",
    canActivate:[authGuard] ,
    loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule)
  },
  { path: '', redirectTo: 'login', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
