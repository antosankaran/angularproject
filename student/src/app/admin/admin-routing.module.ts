import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashbordComponent } from './components/admin-dashbord/admin-dashbord.component';
import { BatchComponent } from './components/batch/batch.component';
import { CoursesComponent } from './components/courses/courses.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { StudentComponent } from './components/student/student.component';
const routes: Routes = [
  {
    path: "",
    component: AdminDashbordComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'student', component: StudentComponent },
      { path: 'batch', component: BatchComponent },
      { path: 'courses', component: CoursesComponent },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },

    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
