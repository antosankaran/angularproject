import { Component,Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BatchService } from '../batch/service/batch.service';


@Component({
  selector: 'app-batchadd',
  templateUrl: './batchadd.component.html',
  styleUrls: ['./batchadd.component.css']
})
export class BatchaddComponent implements OnInit {
 batchForm: FormGroup;
 

 constructor(private bb: FormBuilder,
   private dialogRef: MatDialogRef<BatchaddComponent>, 
   private batchService:BatchService,
   @Inject(MAT_DIALOG_DATA) public data : any   
   ){
 this.batchForm=this.bb.group({
  batch: '',
 });
 }

 ngOnInit(): void {
  this.batchForm.patchValue(this.data)
 }

  onBatchSubmit(){
    if(this.batchForm.valid){
      if(this.data) {
        this.batchService.updateBatch(this.data._id, this.batchForm.value).subscribe({
          next: (_val: any) => {
            alert('batch detail Updated');
            this.dialogRef.close(true);
          },
          error: (err: any) => {
            console.log(err);
          },
        });
      }else{
        this.batchService.addBatch(this.batchForm.value).subscribe({
          next: (_val: any) => {
            alert('batch is successfully');
            this.dialogRef.close(true);
          },
          error: (err: any) => {
            console.log(err);
          },
        });
      }
      
    }
  }
 }

