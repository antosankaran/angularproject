import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchaddComponent } from './batchadd.component';

describe('BatchaddComponent', () => {
  let component: BatchaddComponent;
  let fixture: ComponentFixture<BatchaddComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BatchaddComponent]
    });
    fixture = TestBed.createComponent(BatchaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
