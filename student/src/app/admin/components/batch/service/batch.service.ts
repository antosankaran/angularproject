import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class BatchService {

  constructor(private http: HttpClient) { }

  addBatch(data: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/batch/register`, data);
  }

  getBatch(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/batch/get`);
  }

  deleteBatch(_id: number):Observable<any>{
    return this.http.delete(`${environment.apiUrl}/batch/delete/${_id}`);
  }
  updateBatch(_id: number, data: any):Observable<any>{
    return this.http.patch(`${environment.apiUrl}/batch/patch/${_id}`,data);
  }

}
