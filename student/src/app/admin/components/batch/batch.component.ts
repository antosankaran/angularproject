import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BatchaddComponent } from '../batchadd/batchadd.component';
import { MatTableDataSource } from '@angular/material/table';
import { BatchService } from './service/batch.service';



@Component({
  selector: 'app-batch',
  templateUrl: './batch.component.html',
  styleUrls: ['./batch.component.css']
})
export class BatchComponent implements OnInit {

  displayedColumns: string[] = ['_id', 'batch', 'createdAt', 'updatedAt', 'action'];
  dataSource = new MatTableDataSource<any>;


  constructor(private _dialog: MatDialog, private batchService: BatchService) { }
  ngOnInit(): void {
    this.getBatch();
    // throw new Error('Method not implemented.');
  }
  openBatch() {
   const DialogRef = this._dialog.open(BatchaddComponent);
   DialogRef.afterClosed().subscribe({
    next: (val) =>{
      if(val){
        this.getBatch();
      }
    }
   });
  }

  getBatch() {
    this.batchService.getBatch().subscribe({
      next: (res) => {
        console.log(res);
        this.dataSource = new MatTableDataSource(res);
      }, error: console.log

    })

  }

  updateBatch( data:any) {
   const DialogRef = this._dialog.open(BatchaddComponent,{
      data,
    });
    DialogRef.afterClosed().subscribe({
      next: (val) =>{
        if(val){
          this.getBatch();
        }
      }
     });

  }
  deleteBatch(_id: number) {
    this.batchService.deleteBatch(_id).subscribe({
      next: (res) => {
        // alert("deleted successfully");
        this.getBatch();
      },
      error: console.log
    })
  }



  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
