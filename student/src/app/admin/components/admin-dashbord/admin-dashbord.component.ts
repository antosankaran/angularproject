import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout'


@Component({
  selector: 'app-admin-dashbord',
  templateUrl: './admin-dashbord.component.html',
  styleUrls: ['./admin-dashbord.component.css']
})
export class AdminDashbordComponent {
  sideBarOpen = false;

  constructor(private breakpointObserver: BreakpointObserver) { }

  ngOnInit() {
    this.breakpointObserver.observe([Breakpoints.Handset])
      .subscribe(result => {
        if (result.matches) {
         
          this.sideBarOpen = false;
        } else {
          
          this.sideBarOpen = true;
        }
      });
  }


  sidebartoggle(){
    this.sideBarOpen=!this.sideBarOpen;
  }
}
