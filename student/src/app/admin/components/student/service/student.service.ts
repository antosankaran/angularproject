// student.service.ts

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) { }

  addStudent(data: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/student/register`, data);
  }

  getStudent(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/student/get`);
  }

  deleteStudent(_id: number): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/student/delete/${_id}`);
  }

  // Fix the order of parameters
  updateStudent(_id: number, data: any): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/student/patch/${_id}`, data);
  }
}
