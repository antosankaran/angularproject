import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { StudentaddComponent } from '../studentadd/studentadd.component';
import { StudentService } from './service/student.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  displayedColumns: string[] = ['_id', 'fullName', 'email', 'phoneNumber', 'dob', 'address', 'batch', 'course', 'edit', 'delete'];
  dataSource = new MatTableDataSource<any>;

    getFullName(element:any):string{
      return `${element.firstName} ${element.lastName}`;
    }


 
  constructor(private dialog: MatDialog, private studentService: StudentService) { }

  ngOnInit(): void {
    this.getStudent();
  }

  openmodal() {
    const DialogRef = this.dialog.open(StudentaddComponent);
    DialogRef.afterClosed().subscribe({
      next : (val) => {
        if(val){
          this.getStudent();
        }
      }
    });
  }
 
  //get Student
  getStudent() {
    this.studentService.getStudent().subscribe({
      next: (res) => {
        this.dataSource = new MatTableDataSource(res);
      },
      error: (err) => {
        console.log(err);
      }
    });
  }
  
  //filter and pagination
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  //delete Student
  deleteStudent(_id: number){
this.studentService.deleteStudent(_id).subscribe({
  next: (_res) => {
    alert('Delete Successfully');
    
    this.getStudent();
  },
  error: console.log
})
  }

  //refresh dialog post method

  // openStudent() {
  //   const DialogRef = this.dialog.open(StudentaddComponent);
  //   DialogRef.afterClosed().subscribe({
  //    next: (val) =>{
  //      if(val){
  //        this.getStudent();
  //      }
  //    }
  //   });
  //  }

   //update student

   updateStudent(data: any){
    const DialogRef = this.dialog.open(StudentaddComponent,{
      data,
    });
    DialogRef.afterClosed().subscribe({
      next: (val) =>{
        if(val){
          this.getStudent();
        }
      }
     });
   }

}
