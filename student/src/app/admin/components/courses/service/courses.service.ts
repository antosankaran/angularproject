import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private http: HttpClient) { }
  addCourse(data: any): Observable<any> {
    return this.http.post(`${environment.apiUrl}/course/register`, data);
  }

  getCourse(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/course/get`);
  }

  deleteCourse(_id: number):Observable<any>{
    return this.http.delete(`${environment.apiUrl}/course/delete/${_id}`);
  }
  updateCourse(_id: number,data:any):Observable<any>{
    return this.http.patch(`${environment.apiUrl}/course/patch/${_id}`,data);
  }

}
