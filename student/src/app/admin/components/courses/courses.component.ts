import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { CoursesaddComponent } from '../coursesadd/coursesadd.component';
import { CoursesService } from './service/courses.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent {
  displayedColumns: string[] = ['_id', 'batch', 'createdAt', 'updatedAt', 'action'];
  dataSource = new MatTableDataSource<any>;

  constructor(private _dialog: MatDialog, private coursesService :CoursesService){}
  ngOnInit(): void {
    this.getCourse();
    // throw new Error('Method not implemented.');
  }
  //Get method
  getCourse() {
    this.coursesService.getCourse().subscribe({
      next: (res) => {
        console.log(res);
        this.dataSource = new MatTableDataSource(res);
      }, error: console.log

    })
  }
  updateCourse( data:any) {
    const DialogRef = this._dialog.open(CoursesaddComponent,{
       data,
     });
     DialogRef.afterClosed().subscribe({
       next: (val) =>{
         if(val){
           this.getCourse();
         }
       }
      });
 
   }
   deleteCourse(_id: number) {
    this.coursesService.deleteCourse(_id).subscribe({
      next: (res) => {
        // alert("deleted successfully");
        this.getCourse();
      },
      error: console.log
    })
  }
  openCourse() {
    const DialogRef = this._dialog.open(CoursesaddComponent);
    DialogRef.afterClosed().subscribe({
     next: (val) =>{
       if(val){
         this.getCourse();
       }
     }
    });
   }




   applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  //modal box
modal(){
  this._dialog.open(CoursesaddComponent)
}
//filter

}
