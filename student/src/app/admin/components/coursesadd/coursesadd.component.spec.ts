import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesaddComponent } from './coursesadd.component';

describe('CoursesaddComponent', () => {
  let component: CoursesaddComponent;
  let fixture: ComponentFixture<CoursesaddComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CoursesaddComponent]
    });
    fixture = TestBed.createComponent(CoursesaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
