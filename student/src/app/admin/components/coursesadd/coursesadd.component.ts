import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CoursesService } from '../courses/service/courses.service';

@Component({
  selector: 'app-coursesadd',
  templateUrl: './coursesadd.component.html',
  styleUrls: ['./coursesadd.component.css']
})
export class CoursesaddComponent {
  courseForm: FormGroup;
 

  constructor(private bb: FormBuilder,
    private dialogRef: MatDialogRef<CoursesaddComponent>, 
    private coursesService:CoursesService,
    @Inject(MAT_DIALOG_DATA) public data : any   
    ){
  this.courseForm=this.bb.group({
    course: '',
  });
  }
 
  ngOnInit(): void {
   this.courseForm.patchValue(this.data)
  }
 
   onCourseSubmit(){
     if(this.courseForm.valid){
       if(this.data) {
         this.coursesService.updateCourse(this.data._id, this.courseForm.value).subscribe({
           next: (_val: any) => {
             alert('batch detail Updated');
             this.dialogRef.close(true);
           },
           error: (err: any) => {
             console.log(err);
           },
         });
       }else{
         this.coursesService.addCourse(this.courseForm.value).subscribe({
           next: (_val: any) => {
             alert('batch is successfully');
             this.dialogRef.close(true);
           },
           error: (err: any) => {
             console.log(err);
           },
         });
       }
       
     }
   }
}
