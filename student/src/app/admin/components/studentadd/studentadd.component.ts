import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BatchService } from '../batch/service/batch.service';
import { CoursesService } from '../courses/service/courses.service';
import { StudentService } from '../student/service/student.service';

@Component({
  selector: 'app-studentadd',
  templateUrl: './studentadd.component.html',
  styleUrls: ['./studentadd.component.css']
})
export class StudentaddComponent implements OnInit {
  Form: FormGroup;
  
  _id: string ='';
  Course: any[] = [];
  Batch: any[] = [];

  constructor(private _fb: FormBuilder, private studentService: StudentService,
    private coursesService: CoursesService,
    private batchService: BatchService,
    private dialogRef: MatDialogRef<StudentaddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.Form = this._fb.group({
        firstName: ['', [Validators.required]],
        lastName: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        phoneNumber: ['', [Validators.required]],
        dob: ['', [Validators.required]],
        address: ['', [Validators.required]],
        batch: ['', [Validators.required]],
        course: ['', [Validators.required]],
      });
  }

  ngOnInit(): void {
   // console.log(this.data)
  if(this.data){
    this.Form.patchValue(this.data);
    this.Form.patchValue(
      {batch: this.data.batch._id,
      course: this.data.course._id}
      );
  }   
  // this.Form.patchValue();  
    // console.log(this.data);
  this.loadCourse();
  this.loadBatch();
  }

  loadCourse(){
    this.coursesService.getCourse().subscribe({
      next: (data: any) => {
        this.Course = data;
      }
    })
  }
  
  loadBatch(){
    this.batchService.getBatch().subscribe({
      next: (data: any) => {
        this.Batch = data;
      }
    })
  }
  
  
  onFormSubmit(){
    if(this.Form.valid){
      if(this.data){
        this.studentService.updateStudent(this.data._id, this.Form.value).subscribe({
          next:(val: any) =>{
            // alert('student detail updated');
            // this._coreService.openSnackBar('studentlist detail Updated');
            this.dialogRef.close(true);
          },
          error: (err: any)=>{
            console.log(err)
          }
        });
      }else {
        
        this.studentService.addStudent(this.Form.value).subscribe({
          next:(val: any) =>{
            alert('student added successfully');
            // this._coreService.openSnackBar('StudentList is successfully');
            this.dialogRef.close(true);
          },
          error: (err: any)=>{
            // this._coreService.openSnackBar('This type StudentList is already exists', 'cancel');
            console.log(err)
          }
        });
      }
    }
  }
  }
