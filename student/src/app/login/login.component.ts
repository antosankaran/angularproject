import { HttpClient } from '@angular/common/http';
import {  Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environment/environment';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  hide = true;

  constructor(private http: HttpClient, private router: Router,private authService:AuthService) { 
    //AuthServices
    if(this.authService.isLoggedIn){
      this.router.navigate(['/admin']);
    }
  }

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  })

  get f() {
    return this.form.controls;
  }

 public submit() {
    if (this.form.valid) {
  
      const url = `${environment.apiUrl}/admin/login`;
      const body = {
        email: this.form.value.email,
        password: this.form.value.password
      };
    

      this.http.post<any>(url, body)
        .subscribe((res: any) => {
          if (res) {
            localStorage.setItem('token', res.token);
            console.log('Login Successful');
            this.form.reset();
           
              this.router.navigate(['/admin']);
          } else {
            console.log('User not found');
          }
        }, (_err: any) => {
          //  console.log('Something went wrong');
          console.error(_err);
          
        });
    } else {
      this.form.markAllAsTouched();
    }
  } 
//  public navigateTo(){
//   this.router.navigate(["/dashboard"])
//   }
}
